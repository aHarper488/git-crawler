package Input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import structs.Commit;
import structs.FileChange;
import structs.Pep;
import structs.State;

public class Extractor {

	Map<String, Pep> peps;
	
	public Extractor(){
		peps = new HashMap<String, Pep>();
	}
	
	public void run(){
		
		Reader r = new Reader("test.txt");
		r.run();
		List<FileChange> fileChanges = new ArrayList<FileChange>();
		
		for(Commit c : r.getCommits()){
			fileChanges.addAll(c.getChanges());
		}
		
		for(FileChange f : fileChanges){
			if(f.getStateFlag()){
				mergeMap(f.getName(), extractState(f));
			}
		}
	}
	
	
	private State extractState(FileChange change){
		
		State newState = new State(
				change.getContent().substring(change.getContent().indexOf("\n+Status:") + 10, //end of Status:
						change.getContent().indexOf("\n", change.getContent().indexOf("\n+Status:") + 10)),
				change.getAuthor(),
				change.getDate());
		
		return newState;
	}
	
	private void mergeMap(String fileName, State state){
		
		Pep p = peps.get(fileName);
		if (p == null) { //not found, new thread
			
			Pep newPep = new Pep(fileName, state);
			peps.put(fileName, newPep);
		}
		else { //else merge

			p.addState(state);
			peps.put(fileName, p);
		}
	}
	
	public String getResults(){
		StringBuilder sb = new StringBuilder();
		
		for(Pep p : peps.values()){
			sb.append(p.toString());
		}
		
		return sb.toString();
	}
}
