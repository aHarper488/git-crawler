package structs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class FileChange {
	
	String name;
	String content;
	//TODO move author and date to commit only
	String author;
	String date;
	boolean stateChange;
	
	public FileChange(String name, String author, String date, String content) {

		this.name = name;
		this.content = content;
		this.date = date;
		this.author = findAuthor(author);

		if(content.contains("\n+Status:"))
			stateChange = true;
		else 
			stateChange = false;
	}
	
	private String findAuthor(String author){
		
		String auth = author;
		
		if(content.contains("\n+Author: ")){
			
			//get author line
			author = content.substring(content.indexOf("\n+Author: ") + 10, content.indexOf("\n", content.indexOf("\n+Author: ") + 3)); //+3 gets past inital /n
			author = author.trim();
			//remove <> content
			author = author.replaceAll("<.*>", "");
				
			//split sentence into words
			List<String> authWord = new ArrayList<String>(Arrays.asList(author.split(" ")));
	
			//remove emails
			for (Iterator<String> iter = authWord.listIterator(); iter.hasNext(); ) {
			    String word = iter.next();
			    if (word.contains("@")) {
			        iter.remove();
			    }
			}
			
			
			if(authWord.size() == 1){
				auth = authWord.get(0);
			}
			else{
				//start 1 to skip first name
				StringBuilder sb = new StringBuilder();
				for(int c = 1; c < authWord.size(); c++)
					sb.append(authWord.get(c) + " ");
				auth = sb.toString();
			}
				
			//remove invalid tokens
			auth = auth.replace("<", "");
			auth = auth.replace(">", "");
			auth = auth.replace("(", "");
			auth = auth.replace(")", "");
			auth = auth.replace(",", "");
			
			/*for(String s)
			//get author last name from line
			this.author = author.substring(author.lastIndexOf(" "), author.length() - 1);*/
		}else 
			auth = author;
		
		return auth.trim();
	}
	
	public boolean getStateFlag(){
		return stateChange;
	}
	
	public String getName() {
		return name;
	}
	
	public String getContent() {
		return content;
	}
	
	public String getAuthor() {
		return author;
	}

	public String getDate() {
		return date;
	}

	public String toString(){
		return "------------------------------\n"
				+ "Name: " + name + "\nConent = " + content + "\n";
	}
}
