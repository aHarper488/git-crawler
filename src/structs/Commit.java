package structs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Commit {
	
	String contents;
	String date;
	String author;
	List<FileChange> changes;
	
	private static final String  regPat = "--- .*\\n\\+\\+\\+";
	private static Pattern pattern;
	
	public Commit(String contents) {
		this.contents = contents;
		changes = new ArrayList<FileChange>();
		pattern = Pattern.compile(regPat);
			
		findAuthor();
		findDate();
		findFileChanges();
	}
	
	private void findAuthor(){
		
		//get authors full details including email
		author = contents.substring(contents.indexOf("Author: ") + 8 , contents.indexOf("\n", contents.indexOf("Author: ") + 8));
		//gets last name only
		author = author.substring(author.indexOf(" "), author.indexOf("<") - 1);
	}
	
	private void findDate(){
		
		//gets entire date line
		date = contents.substring(contents.indexOf("Date:   ") + 8 , contents.indexOf("\n", contents.indexOf("Date: ") + 8));
		
		String month = date.substring(4, 7);
		String day = date.substring(8, date.indexOf(" ", 8));	
		String year = date.substring(date.lastIndexOf(" ") - 2, date.lastIndexOf(" ")); 
		
		date = day + "-" + month + "-" + year; 
	}
	
	private void findFileChanges(){
		
		//System.out.println(contents.substring(0, 100));
		
		int startIndex = 0;
		int endIndex = 0;
		final int shift = 5;
		String file;
		String newFile;
		
		if(indexOf(contents) == -1)
				return;
		file = contents.substring(indexOf(contents));
		
		newFile = file.substring(shift);
		
		boolean trigger = true;
		while(trigger){
			endIndex = indexOf(newFile);
			if(endIndex == -1){
				trigger = false;
				endIndex = file.length() - shift;
			}
			String changeContent = file.substring(0, endIndex + shift);
			FileChange addChange= new FileChange(getFileName(changeContent), author, date, changeContent);
			changes.add(addChange);
			if(trigger == false)
				break;
			startIndex = endIndex + shift;
			file = file.substring(endIndex + shift);
			newFile = file.substring(shift);
			
			//System.out.println("file = " + file.length() + "\nnewFile = " +  newFile.length());
		}
	}
	
	private String getFileName(String Content){
		
		int x = Content.indexOf("+++");
		int d = Content.indexOf("\n", Content.indexOf("+++"));
		
		String fileNameLine = Content.substring(x, d);
		return fileNameLine.substring(fileNameLine.lastIndexOf("/"));
	}
	
    /** @return index of pattern in s or -1, if not found */
	private static int indexOf(String s) {
	    Matcher matcher = pattern.matcher(s);
	    return matcher.find() ? matcher.start() : -1;
	}
	
	public String getContents() {
		return contents;
	}

	public List<FileChange> getChanges() {
		return changes;
	}	
	
	public String toString(){
			
		StringBuilder sb = new StringBuilder();
		for(FileChange f : changes){
			sb.append(f.toString());
		}
		
		return sb.toString();
	}
}
