package structs;

public class State {

	String type;
	String author;
	String date;
	
	public State(String type, String author, String date) {
		this.type = type.trim();
		this.author = author;
		this.date = date;
	}
	
	public String getType(){
		return type;
	}
	
	public String toString(){
		return type + ", " + date + ", " + author + ", ";
	}
}
