package structs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pep {
	
	String name;
	String finalStatus;
	String type;
	
	List<State> states;
	
	public Pep(String name){
		this.name = name;
		states = new ArrayList<State>();
	}
	
	public Pep(String name, State state){
		this.name = name;
		states = new ArrayList<State>();
		states.add(state);
	}
	
	public void addState(State add){
		states.add(add);
	}
	
	public void finalize(){
		finalStatus = states.get(states.size() - 1).getType();
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder(name + ", ");
		
		//reverse ordering to match xl
		Collections.reverse(states);
		
		for(State s : states){
			sb.append(s);
		}
		
		sb.append("\n");
		return sb.toString();
	}
}
